# Metallic Chaos

Metallic Chaos is a Kosmo format adaptation of the Skull & Circuits Metal-o-tron II Eurorack module. The link to the original, containing additional information and a demo video, can be found here:

[Skull & Circuits Metal-o-tron II](https://www.skullandcircuits.com/metal-o-tron-ii/?v=d3dcf429c679)

## Version

The current version of this module is v1.1. This version corrects a few minor errors found in v1.0 and includes an optional 4.7k resistor (R89) for changing the decay potentiometer response behavior. According to Skull & Circuits, the inclusion of this resistor changes the taper of the potentiometer and helps to "spread out" the response. I've had a builder of this module prefer not having this resistor in place, so it might be worth testing both with and without it to see what you like best.

I have not built this version of the module yet, but in theory it _should_ work as intended.

## Specs

Width: 10 cm
Power: ±12V, 10-pin
Current: TBD

## Gerbers

The gerber files were generated with the intent of being fabricated by JLCPCB. As such, they were created with JLCPCB's recommendations in mind, notably using the Protel filename extensions and the inclusion of "JLCJLCJLCJLC" on all boards. Other fabricators may have different recommendations/requirements.

## Calibration

Per the Skull & Circuits [build notes](https://www.skullandcircuits.com/wp-content/uploads/2020/06/SC-Metal-O-Tron-II-buildnotes.pdf?v=d3dcf429c679):

"Set the LPF to on, BPF and HPF to OFF
* Turn the Cutoff frequency knob completely counterclockwise (to it’s lowest setting)
* Set the VCA toggle to OFF
* Open up the noise level about halfway
* Now you can adjust the [CV offset] trimmer. You will hear the filter closing, when it’s completely closed,
you’re done."

There's also a trimmer for adjusting the noise level. This is to compensate for variations in transistor noise outputs and can be adjusted to taste.

## Photos

![](images/metallicchaos.jpg)

## Documentation

* [Schematic](schematic/metallicchaos_schematic.pdf) (I apologize for the horrible sizing of the schematic. I may fix it... someday.)
* [BOM](bom/Metallic Chaos v1.1_bom.md)
* [Interactive BOM](bom/Metallic Chaos v1.1 Interactive BOM.html)

## GitLab repository

* [https://gitlab.com/illucynic/metallicchaos](https://gitlab.com/illucynic/metallicchaos)