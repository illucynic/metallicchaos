# Metallic Chaos.kicad_sch BOM

11/11/2023 12:19:16 PM

Generated from schematic by Eeschema 7.0.1

**Component Count:** 191

| Refs | Qty | Component | Description |
| ----- | --- | ---- | ----------- |
| C1–4, C11, C20–38 | 24 | 100nF | Unpolarized capacitor |
| C5 | 1 | 47uF | Polarized capacitor, US symbol |
| C6, C10 | 2 | 10nF | Unpolarized capacitor |
| C7, C8 | 2 | 220nF | Unpolarized capacitor |
| C9 | 1 | 1uF | Unpolarized capacitor |
| C12, C16 | 2 | 150pF | Unpolarized capacitor |
| C13 | 1 | 470nF | Unpolarized capacitor |
| C14 | 1 | 1uF | Polarized capacitor, US symbol |
| C15, C18, C19 | 3 | 10uF | Polarized capacitor, US symbol |
| C17 | 1 | 4.7nF | Unpolarized capacitor |
| D1–8 | 8 | 1N4148 | 100V 0.15A standard switching diode, DO-35 |
| D9 | 1 | LED | Light emitting diode |
| D10, D11 | 2 | 1N5817 | 20V 1A Schottky Barrier Rectifier Diode, DO-41 |
| J1 | 1 | Ext XOR | Audio Jack, 2 Poles (Mono / TS) |
| J2 | 1 | Freq CV In | Audio Jack, 2 Poles (Mono / TS) |
| J3 | 1 | Ext Audio In | Audio Jack, 2 Poles (Mono / TS) |
| J4 | 1 | Velocity In | Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) |
| J5 | 1 | Trig In | Audio Jack, 2 Poles (Mono / TS) |
| J6 | 1 | Decay CV | Audio Jack, 2 Poles (Mono / TS) |
| J7 | 1 | Out | Audio Jack, 2 Poles (Mono / TS) |
| J8 | 1 | Power_2x5 | Generic connector, double row, 02x05, odd/even pin numbering scheme (row 1 odd numbers, row 2 even numbers), script generated (kicad-library-utils/schlib/autogen/connector/) |
| J9 | 1 | Conn_01x06_Female | Generic connector, single row, 01x06, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J10 | 1 | Conn_01x06_Male | Generic connector, single row, 01x06, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J11, J15 | 2 | Conn_01x10_Female | Generic connector, single row, 01x10, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J12, J16 | 2 | Conn_01x10_Male | Generic connector, single row, 01x10, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J13 | 1 | Conn_01x08_Female | Generic connector, single row, 01x08, script generated (kicad-library-utils/schlib/autogen/connector/) |
| J14 | 1 | Conn_01x08_Male | Generic connector, single row, 01x08, script generated (kicad-library-utils/schlib/autogen/connector/) |
| Q1 | 1 | BC550 | 0.1A Ic, 45V Vce, Small Signal NPN Transistor, TO-92 |
| Q2, Q4, Q5, Q8 | 4 | 2N3904 | 0.2A Ic, 40V Vce, Small Signal NPN Transistor, TO-92 |
| Q3 | 1 | J112 | N-JFET transistor, drain/source/gate |
| Q6, Q7 | 2 | 2N3906 | -0.2A Ic, -40V Vce, Small Signal PNP Transistor, TO-92 |
| R1, R3, R5, R7, R10–15, R17, R18, R22, R24, R26–30, R32–35, R37, R38, R40, R41, R44, R49, R54, R69, R73, R78–81 | 36 | 100k | Resistor, US symbol |
| R2, R4, R6, R8, R45, R46, R48, R50, R51, R58, R65, R66, R70 | 13 | 10k | Resistor, US symbol |
| R9, R85, R89 | 3 | 4.7k | Resistor, US symbol |
| R16, R20, R23, R43, R47, R52, R62–64, R74, R88 | 11 | 1k | Resistor, US symbol |
| R19, R39, R53, R55, R59, R61 | 6 | 1M | Resistor, US symbol |
| R21 | 1 | 1.5k | Resistor, US symbol |
| R25, R31, R77 | 3 | 20k | Resistor, US symbol |
| R36, R68 | 2 | 470k | Resistor, US symbol |
| R42, R60 | 2 | 200k | Resistor, US symbol |
| R56, R71 | 2 | 68k | Resistor, US symbol |
| R57, R76, R82, R84, R87 | 5 | 47k | Resistor, US symbol |
| R67 | 1 | 300k | Resistor, US symbol |
| R72 | 1 | 220k | Resistor, US symbol |
| R75, R86 | 2 | 30k | Resistor, US symbol |
| R83 | 1 | 15k | Resistor, US symbol |
| RV1–4, RV9, RV10, RV12 | 7 | B100k | Potentiometer, US symbol |
| RV5 | 1 | 20k | Trim-potentiometer, US symbol |
| RV6 | 1 | B10k | Potentiometer, US symbol |
| RV7, RV8, RV11 | 3 | A100k | Potentiometer, US symbol |
| RV13 | 1 | A1M | Potentiometer, US symbol |
| RV14 | 1 | 100k | Trim-potentiometer, US symbol |
| SW1 | 1 | Decay Short/Long | Switch, single pole double throw |
| SW2 | 1 | HP | Switch, single pole double throw |
| SW3 | 1 | BP | Switch, single pole double throw |
| SW4 | 1 | LP | Switch, single pole double throw |
| SW5 | 1 | VCA On/Off | Switch, single pole double throw |
| U1, U4–6 | 4 | TL074 | Quad Low-Noise JFET-Input Operational Amplifiers, DIP-14/SOIC-14 |
| U2 | 1 | 4070 | Quad Xor 2 inputs |
| U3, U7, U9 | 3 | TL072 | Dual Low-Noise JFET-Input Operational Amplifiers, DIP-8/SOIC-8 |
| U8 | 1 | LM13700 | Dual Operational Transconductance Amplifiers with Linearizing Diodes and Buffers, DIP-16/SOIC-16 |
| U10 | 1 | AS3360 | Dual Voltage Controlled Amplifier (VCA), DIP-14/SOIC-14 |

